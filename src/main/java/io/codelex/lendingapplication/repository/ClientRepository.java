package io.codelex.lendingapplication.repository;


import io.codelex.lendingapplication.client.cliententity.Client;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

@Component
public interface ClientRepository extends CrudRepository<Client, Long> {
    @Query("select count(client) > 0 from Client client where client.email = :email")
    boolean isEmailPresent(@Param("email") String email);

    @Query("select c from Client c where c.email = :email")
    Client findClientByEmail(@Param("email")String email);


}
