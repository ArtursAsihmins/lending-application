package io.codelex.lendingapplication.repository;


import io.codelex.lendingapplication.loan.loanentity.Loan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;


@Component
public interface LoanRepository extends CrudRepository<Loan, String> {
    Loan findLoansById(String id);
}