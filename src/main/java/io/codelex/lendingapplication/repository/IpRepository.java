package io.codelex.lendingapplication.repository;


import io.codelex.lendingapplication.client.cliententity.IP;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface IpRepository extends CrudRepository<IP, Long> {
    Long countByIpAddress(String IP);


}
