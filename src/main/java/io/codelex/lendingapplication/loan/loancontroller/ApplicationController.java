package io.codelex.lendingapplication.loan.loancontroller;


import io.codelex.lendingapplication.loan.loandto.*;
import io.codelex.lendingapplication.loan.loanentity.Extension;
import io.codelex.lendingapplication.loan.loanentity.Loan;
import io.codelex.lendingapplication.loan.loanservice.ApplicationService;
import io.codelex.lendingapplication.loan.loanservice.ProductConstraintsService;
import io.codelex.lendingapplication.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;

@RestController
@RequestMapping("/api")
public class ApplicationController {


    private ProductConstraintsService productConstraintsService;
    private ApplicationService applicationService;
    private LoanRepository loanRepository;

    @Autowired
    public ApplicationController(ProductConstraintsService productConstraintsService, ApplicationService applicationService, LoanRepository loanRepository) {
        this.productConstraintsService = productConstraintsService;
        this.applicationService = applicationService;
        this.loanRepository = loanRepository;
    }

    @PostMapping("/loans/apply")
    public StatusDTO loansAccreditation(@RequestBody ClientApplicationDTO clientApplicationDTO, Principal principal, HttpServletRequest request) {
        return applicationService.applyForLoan(clientApplicationDTO, principal, request);
    }


    @GetMapping("/loans")
    public Iterable<LoanDTO> showLoans(Principal principal) {
        return applicationService.getLoans(principal);
    }


    @PostMapping("/loans/{loan-id}/extend?days")
    public Extension loanExtension(@PathVariable("loan-id") String id, @RequestParam("days") Integer days) {
        return applicationService.extendLoan(id, days);
    }


    @GetMapping("/constraints")
    public ProductConstraintsDTO constraints() {
        return productConstraintsService.constraints();
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(IllegalStateException.class)
    void handleIllegalState() {
    }


    @ResponseStatus(CONFLICT)
    @ExceptionHandler(IllegalArgumentException.class)
    void handleIllegalArgument() {
    }
}
