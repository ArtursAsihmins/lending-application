package io.codelex.lendingapplication.loan.loanservice;

import java.math.BigDecimal;

import static java.awt.Event.DOWN;
import static java.math.RoundingMode.HALF_DOWN;

public class InterestRate {
    private static final BigDecimal INTEREST_RATE_PER_MONTH = new BigDecimal("0.1");
    private static final BigDecimal EXTENSION_INTEREST_COEFF = new BigDecimal("0.1");
    private static final int DAYS_IN_MONTH = 30;

    BigDecimal calculate(BigDecimal principal, Integer days) {
        BigDecimal monthlyInterest = principal.setScale(3, HALF_DOWN).multiply(INTEREST_RATE_PER_MONTH);
        return monthlyInterest
                .divide(new BigDecimal(DAYS_IN_MONTH).setScale(3, HALF_DOWN), DOWN)
                .multiply(new BigDecimal(days))
                .setScale(2, HALF_DOWN);
    }

    BigDecimal calculateExtensionInterest(BigDecimal principal, Integer days) {
        BigDecimal interest = calculate(principal, days);
        return interest.add(interest.multiply(EXTENSION_INTEREST_COEFF)).setScale(2, HALF_DOWN);
    }
}
