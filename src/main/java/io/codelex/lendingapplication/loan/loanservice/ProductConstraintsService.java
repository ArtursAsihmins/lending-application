package io.codelex.lendingapplication.loan.loanservice;

import io.codelex.lendingapplication.loan.loandto.*;
import org.springframework.stereotype.Component;


import java.math.BigDecimal;

@Component
public class ProductConstraintsService {


    private BigDecimal minAmount = BigDecimal.valueOf(100);
    private BigDecimal maxAmount = BigDecimal.valueOf(500);
    private Integer minTermDays = 7;
    private Integer maxTermDays = 30;
    private Integer minExtensionDays = 7;
    private Integer maxExtensionDays = 30;

    public ProductConstraintsDTO constraints() {
        ProductConstraintsDTO productConstraintsDTO = new ProductConstraintsDTO();
        productConstraintsDTO.setMinAmount(minAmount);
        productConstraintsDTO.setMaxAmount(maxAmount);
        productConstraintsDTO.setMinTermDays(minTermDays);
        productConstraintsDTO.setMaxTermDays(maxTermDays);
        productConstraintsDTO.setMinExtensionDays(minExtensionDays);
        productConstraintsDTO.setMaxExtensionDays(maxExtensionDays);


        return productConstraintsDTO;
    }

    public boolean addConstraints(ClientApplicationDTO clientApplicationDTO) {
        boolean checkStatus = true;
        if (clientApplicationDTO.getDays() > maxTermDays ||
                clientApplicationDTO.getDays() < minTermDays ||
                clientApplicationDTO.getAmount().compareTo(maxAmount) > 0 ||
                clientApplicationDTO.getAmount().compareTo(minAmount) < 0 ){
            checkStatus = false;
        }

        return checkStatus;
    }

    public boolean addConstraintsToExtension(ExtensionDTO extensionDTO) {
        boolean checkStatus = true;
        if(extensionDTO.getDays() > maxExtensionDays ||
                extensionDTO.getDays() < minExtensionDays){
            checkStatus = false;
        }
        return checkStatus;
    }
}
