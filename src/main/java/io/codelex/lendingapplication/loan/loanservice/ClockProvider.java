package io.codelex.lendingapplication.loan.loanservice;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


@Component
public class ClockProvider {


    public LocalDateTime now = LocalDateTime.now();

    public void setCurrentTime(LocalDateTime now) {
        this.now = now;

    }

    public void resetCurrentTime() {
        this.now = LocalDateTime.now();
    }

    public LocalDateTime getCurrentTime() {
        return this.now;
    }
}
