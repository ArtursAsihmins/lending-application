package io.codelex.lendingapplication.loan.loanservice;

import io.codelex.lendingapplication.client.cliententity.Client;
import io.codelex.lendingapplication.loan.loandto.*;
import io.codelex.lendingapplication.loan.loanentity.Extension;
import io.codelex.lendingapplication.loan.loanentity.Loan;
import io.codelex.lendingapplication.loan.loanmapper.LoanMapper;
import io.codelex.lendingapplication.repository.ClientRepository;
import io.codelex.lendingapplication.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static io.codelex.lendingapplication.loan.loandto.Status.APPROVED;
import static io.codelex.lendingapplication.loan.loandto.Status.REJECTED;


@Component
public class ApplicationService {

    private LoanMapper loanMapper;
    private ClientRepository clientRepository;
    private LoanRepository loanRepository;
    private RiskAssessmentService riskAssessmentService;
    private ProductConstraintsService productConstraintsService;

    @Autowired
    public ApplicationService(LoanMapper loanMapper, ClientRepository clientRepository, LoanRepository loanRepository, RiskAssessmentService riskAssessmentService, ProductConstraintsService productConstraintsService) {
        this.loanMapper = loanMapper;
        this.clientRepository = clientRepository;
        this.loanRepository = loanRepository;
        this.riskAssessmentService = riskAssessmentService;
        this.productConstraintsService = productConstraintsService;
    }

    public StatusDTO applyForLoan(ClientApplicationDTO clientApplicationDTO, Principal principal, HttpServletRequest request) {
        if (clientApplicationDTO.getDays() == null ||
                clientApplicationDTO.getAmount() == null) {
            throw new IllegalStateException();
        }

        StatusDTO statusDTO = riskAssessmentService.checkApplication(clientApplicationDTO, request.getRemoteAddr());
        String email = principal.getName().toLowerCase().trim();
        Client client = clientRepository.findClientByEmail(email);
        Loan loan = loanMapper.loanDtoToEntity(clientApplicationDTO);
        List<Loan> loans = client.getLoanList();
        for (Loan viewLoan : loans) {
            if (viewLoan.getStatus().equals(LoanStatus.OPEN)) {
                throw new IllegalArgumentException();
            }
        }

        if (!productConstraintsService.addConstraints(clientApplicationDTO)) {
            throw new IllegalStateException();
        }
        if (statusDTO.getStatus().equals(APPROVED)) {
            loan.setStatus(LoanStatus.OPEN);
            loans.add(loan);
            client.setLoanList(loans);
            clientRepository.save(client);
            loanRepository.save(loan);

        } else {
            statusDTO.setStatus(REJECTED);
        }

        return statusDTO;
    }


    public Extension extendLoan(String id, Integer days) {
        Loan loan = loanRepository.findLoansById(id);
        Extension extension = loanMapper.extendLoanDTOToEntity(days);

        return extension;

    }

    public Iterable<LoanDTO> getLoans(Principal principal) {
        String email = principal.getName().toLowerCase().trim();
        Client client = clientRepository.findClientByEmail(email);
        List<LoanDTO> loanDTOList = new ArrayList<>();
        for (Loan view : client.getLoanList()) {
            LoanDTO loanDTO = loanMapper.loanEntityToDTO(view);
            loanDTOList.add(loanDTO);
        }


        return loanDTOList;

    }


}





