package io.codelex.lendingapplication.loan.loanservice;


import io.codelex.lendingapplication.client.clientdto.ClientDTO;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;


@Component
public class AuthenticationService {

    public void authorise(ClientDTO clientDTO, String role) {
        List<SimpleGrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority(role));
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(clientDTO.getEmail(), null, authorities);
        getContext().setAuthentication(token);

    }


    public void clearAuthentication() {
        getContext().setAuthentication(null);
    }

}