package io.codelex.lendingapplication.loan.loanservice;


import io.codelex.lendingapplication.client.cliententity.IP;
import io.codelex.lendingapplication.loan.loandto.ClientApplicationDTO;
import io.codelex.lendingapplication.loan.loandto.ProductConstraintsDTO;
import io.codelex.lendingapplication.loan.loandto.Status;
import io.codelex.lendingapplication.loan.loandto.StatusDTO;
import io.codelex.lendingapplication.repository.IpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalTime;


@Component
public class RiskAssessmentService {
    IpRepository ipRepository;
    ClockProvider clockProvider;
    ProductConstraintsService productConstraintsService;

    @Autowired
    public RiskAssessmentService(IpRepository ipRepository, ClockProvider clockProvider, ProductConstraintsService productConstraintsService) {
        this.ipRepository = ipRepository;
        this.clockProvider = clockProvider;
        this.productConstraintsService = productConstraintsService;
    }


    public boolean rejectByTime(ClientApplicationDTO clientApplicationDTO) {
        ProductConstraintsDTO productConstraintsDTO = productConstraintsService.constraints();
        LocalTime time = clockProvider.getCurrentTime().toLocalTime();
        if (clientApplicationDTO.getAmount().compareTo(productConstraintsDTO.getMaxAmount()) >=0) {


            if (time.getHour() >=21 || time.getHour() <5) {
                return true;
            }

        }

        return false;
    }

    public boolean rejectByIpAddress(String ipAddress) {
        IP ip = new IP();
        ip.setIpAddress(ipAddress);
        ipRepository.save(ip);
        Long ipAddressCount = ipRepository.countByIpAddress(ipAddress);
        if (ipAddressCount >= 3) {
            return true;
        }
        return false;

    }


    public StatusDTO checkApplication(ClientApplicationDTO clientApplicationDTO, String ipAddress) {
        StatusDTO statusDTO = new StatusDTO();
//        if (rejectByIpAddress(ipAddress)) {
//            statusDTO.setStatus(Status.REJECTED);
//            return statusDTO;
//        }
        if (rejectByTime(clientApplicationDTO)) {
            statusDTO.setStatus(Status.REJECTED);
            return statusDTO;
        }

        statusDTO.setStatus(Status.APPROVED);


        return statusDTO;
    }


}




