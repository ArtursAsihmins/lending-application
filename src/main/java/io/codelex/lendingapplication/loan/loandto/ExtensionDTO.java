package io.codelex.lendingapplication.loan.loandto;


import java.math.BigDecimal;
import java.time.LocalDate;

public class ExtensionDTO {


    private LocalDate created;
    private Integer days;
    private BigDecimal interest;

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }
}
