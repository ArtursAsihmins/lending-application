package io.codelex.lendingapplication.loan.loandto;

public enum Status {

    APPROVED, REJECTED
}
