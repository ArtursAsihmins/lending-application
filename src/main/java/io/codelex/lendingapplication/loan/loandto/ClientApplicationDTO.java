package io.codelex.lendingapplication.loan.loandto;

import java.math.BigDecimal;

public class ClientApplicationDTO {


    private BigDecimal amount;
    private Integer days;


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }
}
