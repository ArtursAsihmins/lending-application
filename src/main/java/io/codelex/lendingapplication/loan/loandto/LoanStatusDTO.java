package io.codelex.lendingapplication.loan.loandto;

public class LoanStatusDTO {


    LoanStatus loanStatus;


    public LoanStatus getLoanStatus() {
        return loanStatus;
    }


    public void setLoanStatus(LoanStatus loanStatus) {
        this.loanStatus = loanStatus;
    }
}
