package io.codelex.lendingapplication.loan.loanmapper;


import io.codelex.lendingapplication.loan.loandto.ClientApplicationDTO;
import io.codelex.lendingapplication.loan.loandto.ExtensionDTO;
import io.codelex.lendingapplication.loan.loandto.LoanDTO;
import io.codelex.lendingapplication.loan.loanentity.Extension;
import io.codelex.lendingapplication.loan.loanentity.Loan;
import io.codelex.lendingapplication.loan.loanservice.ClockProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Random;


@Component
public class LoanMapper {

    private ClockProvider clockProvider;

    @Autowired
    public LoanMapper(ClockProvider clockProvider) {
        this.clockProvider = clockProvider;
    }

    public LoanMapper() {
    }


    public Loan loanDtoToEntity(ClientApplicationDTO clientApplicationDTO) {
        Random random = new Random();
        Loan loan = new Loan();
        String id = random.nextInt(10000) + "-" + random.nextInt(10000);
        LocalDate created = clockProvider.getCurrentTime().toLocalDate();
        LocalDate dueDate = created.plusDays(clientApplicationDTO.getDays());
        BigDecimal principal = clientApplicationDTO.getAmount();
        BigDecimal interest = principal.divide(BigDecimal.valueOf(10));
        BigDecimal total = principal.add(interest);

        loan.setId(id);
        loan.setStatus(loan.getStatus());
        loan.setCreated(created);
        loan.setDueDate(dueDate);
        loan.setPrincipal(principal);
        loan.setInterest(interest);
        loan.setTotal(total);

        return loan;
    }

    public Extension extendLoanDTOToEntity(Integer days) {
        Extension extension = new Extension();
        BigDecimal interest = applyInterest(days);

        extension.setDays(days);
        extension.setCreated(clockProvider.getCurrentTime().toLocalDate());
        extension.setInterest(interest);

        return extension;
    }

    public BigDecimal applyInterest(Integer days) {
        Extension extension = extendLoanDTOToEntity(days);
        Loan loan = new Loan();
        Integer interest = days;

        for (int i = interest; i < 31; i++) {
            if (i == 7) {
                extension.setInterest(loan.getTotal().multiply(BigDecimal.valueOf(0.025)));
            } else if (i > 8 && i < 15) {
                extension.setInterest(loan.getTotal().multiply(BigDecimal.valueOf(0.05)));
            } else if (i > 15 && i < 22) {
                extension.setInterest(loan.getTotal().multiply(BigDecimal.valueOf(0.075)));
            } else if (i > 22 && i < 31) {
                extension.setInterest(loan.getTotal().multiply(BigDecimal.valueOf(0.1)));
            }
        }
        return BigDecimal.valueOf(interest);

    }

    public LoanDTO loanEntityToDTO(Loan loan) {
        LoanDTO loanDTO = new LoanDTO();

        loanDTO.setId(loan.getId());
        loanDTO.setStatus(loan.getStatus());
        loanDTO.setCreated(loan.getCreated());
        loanDTO.setDueDate(loan.getDueDate());
        loanDTO.setPrincipal(loan.getPrincipal());
        loanDTO.setInterest(loan.getInterest());
        loanDTO.setTotal(loan.getTotal());

        return loanDTO;
    }

    public ExtensionDTO extensionEntityToDTO(Extension extension) {
        ExtensionDTO extensionDTO = new ExtensionDTO();

        extension.setDays(extension.getDays());
        extension.setCreated(extension.getCreated());
        extension.setInterest(extension.getInterest());

        return extensionDTO;
    }

}







