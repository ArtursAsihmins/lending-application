package io.codelex.lendingapplication.testing;

import io.codelex.lendingapplication.repository.ClientRepository;
import io.codelex.lendingapplication.repository.IpRepository;
import io.codelex.lendingapplication.repository.LoanRepository;
import org.springframework.stereotype.Component;


@Component
public class TestingService {


    private IpRepository ipRepository;
    private ClientRepository clientRepository;
    private LoanRepository loanRepository;

    public TestingService(IpRepository ipRepository, ClientRepository clientRepository, LoanRepository loanRepository) {
        this.ipRepository = ipRepository;
        this.clientRepository = clientRepository;
        this.loanRepository = loanRepository;
    }

    public void clearDatabase() {
        ipRepository.deleteAll();
        clientRepository.deleteAll();
        loanRepository.deleteAll();

    }
}
