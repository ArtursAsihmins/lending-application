package io.codelex.lendingapplication.testing;

public class Helicopter {

    private int height;
    private int x;
    private int y;

    public void flyUp() {
        height++;
    }

    public int getHeight() {
        return height;
    }

    public void flyDown() {
        if (height == 0) {
            return;
        }
        height--;
    }


    public void flyForward() {
        x++;

        if (y >= 0) {

            y++;
        }
            else
                y--;
            }




    public int getX() {
        return x;
    }

    public void flyBackwards() {
        x--;
    }

    public int getY() {
        {
            return y;
        }
    }

    public void flyLeft() {
        y++;
    }

    public void flyRight() {
        y--;
    }

}


