package io.codelex.lendingapplication.testing;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Order(150)
@Configuration
public class TestSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/testing-api/**")
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/testing-api/**").permitAll();

    }


}


