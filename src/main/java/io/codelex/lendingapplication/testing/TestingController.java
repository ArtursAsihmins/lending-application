package io.codelex.lendingapplication.testing;


import io.codelex.lendingapplication.loan.loanservice.AuthenticationService;
import io.codelex.lendingapplication.loan.loanservice.ClockProvider;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@RequestMapping("/testing-api")
public class TestingController {


    private final ClockProvider clockProvider;
    private final AuthenticationService authenticationService;
    private final TestingService testingService;

    public TestingController(ClockProvider clockProvider, AuthenticationService authenticationService, TestingService testingService) {
        this.clockProvider = clockProvider;
        this.authenticationService = authenticationService;
        this.testingService = testingService;
    }

    @PostMapping("/reset-time")
    public void resetTime() {
        clockProvider.resetCurrentTime();
    }

    @PutMapping("/time")
    public void setTime(@RequestBody LocalDateTime now) {
        clockProvider.setCurrentTime(now);
    }

    @GetMapping("/time")
    public LocalDateTime getTime() {
        return clockProvider.getCurrentTime();
    }

    @PostMapping("/clear-database")
    public void clearDatabase() {
        testingService.clearDatabase();

    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    void handleIllegalArgument() {

    }
}
