package io.codelex.lendingapplication.client.clientservice;

import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class PasswordValidation {

        private Pattern pattern;
        private Matcher matcher;

        private static final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=\\S+$).{4,}$";

        public PasswordValidation() {
            pattern = Pattern.compile(PASSWORD_PATTERN);
        }

        public boolean validate(final String password) {

            matcher = pattern.matcher(password);
            return matcher.matches();

        }
    }
