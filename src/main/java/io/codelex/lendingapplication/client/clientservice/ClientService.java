package io.codelex.lendingapplication.client.clientservice;


import io.codelex.lendingapplication.client.clientdto.ClientDTO;
import io.codelex.lendingapplication.client.cliententity.Client;
import io.codelex.lendingapplication.repository.ClientRepository;
import io.codelex.lendingapplication.loan.loanservice.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;


@Component
public class ClientService {


    private ClientRepository clientRepository;
    private EmailValidation emailValidation;
    private AuthenticationService authenticationService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private PasswordValidation passwordValidation;

    @Autowired
    public ClientService(ClientRepository clientRepository, EmailValidation emailValidation, AuthenticationService authenticationService, BCryptPasswordEncoder bCryptPasswordEncoder, PasswordValidation passwordValidation) {
        this.clientRepository = clientRepository;
        this.emailValidation = emailValidation;
        this.authenticationService = authenticationService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.passwordValidation = passwordValidation;
    }

    public Iterable<Client> showLoans() {
        return clientRepository.findAll();
    }


    @Transactional
    public void register(ClientDTO clientDTO) {
        String email = clientDTO.getEmail().toLowerCase().trim();
        if (clientDTO.getEmail() == null) {
            throw new IllegalStateException();
        }
        if (clientRepository.isEmailPresent(email)) {
            throw new IllegalStateException();
        } else {

            if (emailValidation.validate(email)) {
                authenticationService.authorise(clientDTO, "ROLE_CLIENT");
                Client client = new Client(clientDTO.getEmail(), clientDTO.getPassword());
                clientDTO.setPassword(bCryptPasswordEncoder.encode(clientDTO.getPassword()));
                client.setEmail(email);
                clientRepository.save(client);
            }

        }
    }


    @Transactional
    public void signIn(ClientDTO clientDTO) {
        if (clientDTO.getEmail() == null || clientDTO.getPassword() == null) {
            throw new IllegalStateException();
        }
        String email = clientDTO.getEmail().toLowerCase().trim();
        if (!emailValidation.validate(email) || !clientRepository.isEmailPresent(email)) {
            throw new IllegalStateException();
        }
        Client client = clientRepository.findClientByEmail(email);
        String password = clientDTO.getPassword();
        if (!passwordValidation.validate(password)) {
            throw new IllegalArgumentException();
        }
        if (client.getPassword().equals(password)) {
            authenticationService.authorise(clientDTO, "ROLE_CLIENT");
        } else {
            throw new IllegalArgumentException();
        }
    }


}










