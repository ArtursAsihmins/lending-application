package io.codelex.lendingapplication.client.clientcontroller;


import io.codelex.lendingapplication.client.clientdto.ClientDTO;
import io.codelex.lendingapplication.client.clientservice.ClientService;
import io.codelex.lendingapplication.loan.loanservice.AuthenticationService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
class ClientController {


    private final AuthenticationService authenticationService;
    private final ClientService clientService;


    ClientController(AuthenticationService authenticationService, ClientService clientService) {
        this.authenticationService = authenticationService;
        this.clientService = clientService;
    }


    @PostMapping("/sign-in")
    public void signIn(@Valid @RequestBody ClientDTO clientDTO) {
        clientService.signIn(clientDTO);
    }


    @PostMapping("/register")
    public void register(@Valid @RequestBody ClientDTO clientDTO) {
        clientService.register(clientDTO);
        authenticationService.authorise(clientDTO, "ROLE_CLIENT");
    }


    @PostMapping("/sign-out")
    public void signOut() {
        authenticationService.clearAuthentication();
    }


    @GetMapping("/account")
    public String account(Principal principal) {
        return principal.getName();
    }


    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(IllegalStateException.class)
    void handleIllegalState() {
    }


    @ResponseStatus(CONFLICT)
    @ExceptionHandler(IllegalArgumentException.class)
    void handleIllegalArgument() {
    }


}
