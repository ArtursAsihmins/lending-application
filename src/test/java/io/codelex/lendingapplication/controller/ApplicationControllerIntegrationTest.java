package io.codelex.lendingapplication.controller;


import io.codelex.lendingapplication.loan.loanservice.ApplicationService;
import io.codelex.lendingapplication.loan.loanservice.ProductConstraintsService;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationControllerIntegrationTest {

    @Autowired
    ProductConstraintsService productConstraintsService;
    @Autowired
    ApplicationService applicationService;
}
