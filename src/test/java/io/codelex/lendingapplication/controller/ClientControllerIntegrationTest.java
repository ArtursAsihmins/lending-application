package io.codelex.lendingapplication.controller;

import io.codelex.lendingapplication.client.clientdto.ClientDTO;
import io.codelex.lendingapplication.client.cliententity.Client;
import io.codelex.lendingapplication.repository.ClientRepository;
import io.codelex.lendingapplication.loan.loanservice.AuthenticationService;
import io.codelex.lendingapplication.client.clientservice.ClientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientControllerIntegrationTest {

    @Autowired
    AuthenticationService authenticationService;
    @Autowired
    ClientService clientService;
    @Autowired
    ClientRepository clientRepository;

    @Test
    public void shouldSaveNewClient(){
ClientDTO clientDTO = new ClientDTO();
clientDTO.setEmail("example@codelex.io");
clientDTO.setPassword("12345");
clientService.register(clientDTO);
authenticationService.authorise(clientDTO, "ROLE_CLIENT");

Client client =clientRepository.findClientByEmail(clientDTO.getEmail());

assertNotNull(client);
    }

    @Test
    public void shouldSignIn() {
        //given
        ClientDTO clientDTO = new ClientDTO();
        ClientRepository clientRepositoryMock = Mockito.mock(ClientRepository.class);
        Mockito.when(clientRepositoryMock.isEmailPresent("example@codelex.io"));
        authenticationService.authorise(clientDTO, "ROLE_CLIENT");
        //when


        //then


    }

    @Test
    public void shouldSignOut() {

    }

    @Test
    public void shouldViewAccount() {

    }
}
