package io.codelex.lendingapplication.repository;


import io.codelex.lendingapplication.client.cliententity.IP;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
@DataJpaTest
@RunWith(SpringRunner.class)
public class IpRepositoryTest {

    @Autowired
    IpRepository ipRepository;
   IP ip;


    @Test
    public void shouldBeEmpty() {
        assertEquals(ipRepository.count(), 0);
    }

    public void findByIpAddress() {
        ip.setIpAddress("0.0.0.0");
        ipRepository.save(ip);
        assertEquals(ipRepository.findAll(),1);

    }
}