package io.codelex.lendingapplication.service;

import io.codelex.lendingapplication.loan.loandto.ClientApplicationDTO;
import io.codelex.lendingapplication.loan.loandto.StatusDTO;
import io.codelex.lendingapplication.loan.loanservice.ClockProvider;
import io.codelex.lendingapplication.loan.loanmapper.LoanMapper;
import io.codelex.lendingapplication.loan.loanservice.ProductConstraintsService;
import io.codelex.lendingapplication.loan.loanservice.RiskAssessmentService;
import io.codelex.lendingapplication.repository.IpRepository;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.*;

public class RiskAssessmentServiceTest {
    ClockProvider clockProvider = new ClockProvider();
    ProductConstraintsService productConstraintsService = new ProductConstraintsService();
    IpRepository ipRepositorymock = Mockito.mock(IpRepository.class);
    RiskAssessmentService riskAssessmentService = new RiskAssessmentService(ipRepositorymock, clockProvider, productConstraintsService);
    ClientApplicationDTO clientApplicationDTO = new ClientApplicationDTO();


    @Test
    public void shouldApproveWithMinimalAmountAtDay() {
        //given
        clientApplicationDTO.setAmount(BigDecimal.valueOf(100));
        clientApplicationDTO.setDays(30);
        clockProvider.setCurrentTime(LocalDateTime.of(2019, 1, 1, 10, 0));

        //when
        boolean isRejected = riskAssessmentService.rejectByTime(clientApplicationDTO);

        //then
        assertFalse(isRejected);
    }


    @Test
    public void shouldApproveWithMaximalAmountInTheMiddleOfTheDay(){

        //given
        clientApplicationDTO.setAmount(BigDecimal.valueOf(500));
        clientApplicationDTO.setDays(30);
        clockProvider.setCurrentTime(LocalDateTime.of(2019, 1, 1, 15, 0));

        //when
        boolean isRejected = riskAssessmentService.rejectByTime(clientApplicationDTO);

        //then
        assertFalse(isRejected);
    }

    @Test
    public void shouldRejectWithMaximalAmountAtNight(){
        //given
        clientApplicationDTO.setAmount(BigDecimal.valueOf(500));
        clientApplicationDTO.setDays(30);
        clockProvider.setCurrentTime(LocalDateTime.of(2019, 1, 1, 21, 0));

        //when
        boolean isRejected = riskAssessmentService.rejectByTime(clientApplicationDTO);

        //then
        assertTrue(isRejected);
    }



    @Test
    public void shouldApproveWithMinimalAmountAtNight() {
        //given
        clientApplicationDTO.setAmount(BigDecimal.valueOf(100));
        clientApplicationDTO.setDays(30);
        clockProvider.setCurrentTime(LocalDateTime.of(2019, 1, 1, 23, 0));

        //when
        boolean isRejected = riskAssessmentService.rejectByTime(clientApplicationDTO);

        //then
        assertFalse(isRejected);
    }
}
