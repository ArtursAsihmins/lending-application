package io.codelex.lendingapplication;

import io.codelex.lendingapplication.testing.Helicopter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HelicopterTest {
    //fly down/up
    //fly left/right
    //fly forward/backwards

    Helicopter helicopter = new Helicopter();

    @Test
    public void shouldFlyUp() {

        //when
        helicopter.flyUp();
        //then
        assertEquals(1, helicopter.getHeight());
    }

    @Test
    public void shouldFlyDown() {

        //when
        helicopter.flyUp();
        helicopter.flyUp();
        helicopter.flyDown();
        //then
        assertEquals(1, helicopter.getHeight());
    }

    @Test
    public void shouldFlyUpMultipleTimes() {

        //when
        helicopter.flyUp();
        helicopter.flyUp();
        helicopter.flyUp();
        //then
        assertEquals(3, helicopter.getHeight());
    }

    @Test
    public void shouldNotBeAbleToGetBelowTheGround() {
        //when
        helicopter.flyDown();

        //then
        assertEquals(0, helicopter.getHeight());
    }

    @Test
    public void shouldFlyForward() {
        //when
        helicopter.flyForward();

        //then
        assertEquals(1, helicopter.getX());
    }

    @Test
    public void shouldFlyBackwards() {
        //when
        helicopter.flyBackwards();

        //then
        assertEquals(-1, helicopter.getX());
    }

    @Test
    public void shouldFlyToTheLeft() {
        //when
        helicopter.flyLeft();

        //then
        assertEquals(1, helicopter.getY());
    }

    @Test
    public void shouldFlyToTheRight() {
        //when
        helicopter.flyRight();

        //then
        assertEquals(-1, helicopter.getY());
    }


    @Test
    public void should_fly_dependent_of_location() {
        //when
        helicopter.flyLeft();
        helicopter.flyForward();
        helicopter.flyForward();
        helicopter.flyForward();





        //then
        assertEquals(4, helicopter.getY());
    }
}
