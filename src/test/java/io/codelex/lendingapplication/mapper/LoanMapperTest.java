package io.codelex.lendingapplication.mapper;

import io.codelex.lendingapplication.loan.loandto.ClientApplicationDTO;
import io.codelex.lendingapplication.loan.loandto.LoanStatus;
import io.codelex.lendingapplication.loan.loandto.LoanStatusDTO;
import io.codelex.lendingapplication.loan.loandto.StatusDTO;
import io.codelex.lendingapplication.loan.loanentity.Loan;
import io.codelex.lendingapplication.loan.loanmapper.LoanMapper;
import io.codelex.lendingapplication.loan.loanservice.ClockProvider;
import io.codelex.lendingapplication.loan.loanservice.ProductConstraintsService;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Random;


public class LoanMapperTest {


    Loan loan = new Loan();
    Random random = new Random();

    @Test
    public void applyForLoan() {



//given
        String id = random.nextInt(10000)+ "-" + random.nextInt(10000);
        LocalDate created = LocalDate.now();
        LocalDate dueDate = created.plusDays(30);
        BigDecimal principal = BigDecimal.valueOf(500);
        BigDecimal interest = principal.divide(BigDecimal.valueOf(10));
        BigDecimal total = principal.add(interest);

// when
        loan.setId(id);
//        loan.setStatus(loan.getStatus());
        loan.setCreated(created);
        loan.setDueDate(dueDate);
        loan.setPrincipal(principal);
        loan.setInterest(interest);
        loan.setTotal(total);


//then
        Assert.assertEquals(loan.getId(),id);
//        Assert.assertEquals(loan.getStatus(), loanStatusDTO);
        Assert.assertEquals(loan.getCreated(), created);
        Assert.assertEquals(loan.getDueDate(), dueDate);
        Assert.assertEquals(loan.getPrincipal(), BigDecimal.valueOf(500));
        Assert.assertEquals(loan.getInterest(), BigDecimal.valueOf(50));
        Assert.assertEquals(loan.getTotal(), BigDecimal.valueOf(550));

    }

}




